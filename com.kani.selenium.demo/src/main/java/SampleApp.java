import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class SampleApp
{

    /**
     * @param args
     */
    public static void main(String[] args)
    {
        WebDriver driver = null;
        try
        {
            // set properties
            System.setProperty("webdriver.firefox.bin",
                    "/Users/kdesilva/Desktop/old_firefox/Firefox.app/Contents/MacOS/firefox");

            // get preffered web driver implementation
            driver = new FirefoxDriver();
            driver.get("http://www.ask.com");

            WebElement searchBox = driver.findElement(By.xpath("//input[@id='q!!!!']"));
            searchBox.sendKeys("Sri Lanka");

            WebElement searchBtn = driver.findElement(By.xpath("//input[@id='sbut']"));
            searchBtn.click();

            WebElement resultPageTxtBox = driver.findElement(By.xpath("//input[@id='top_q_comm']"));
            System.out.println(resultPageTxtBox.getAttribute("value"));
        }
        catch (NoSuchElementException e)
        {
            System.err.println("Could not find the element" + e.getMessage());
        }
        catch (Exception e)
        {
            System.err.println("Unexpected Error!");
        }
        finally
        {
            if (driver != null)
            {
                driver.close();
            }
        }

    }
}
