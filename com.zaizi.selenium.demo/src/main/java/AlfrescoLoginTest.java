import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class AlfrescoLoginTest
{
    // login page
    private static final String LOGIN_PAGE = "http://192.168.1.53:8080/share";
    private static final String USERNAME = "admin";
    private static final String USERNAME_TEXTFIELD = "//input[@id='username' and @type='text']";
    private static final String PASSWORD = "admin";
    private static final String PASSWORD_TEXTFIELD = "//input[@id='password' and @type='password']";
    private static final String LOGIN_BUTTON = "//input[@id='btn-login' and @type='submit']";

    // user dashboard elements
    private static final String USERNAME_AREA = "//button[contains(@id,'default-user_user-button') and @type='button']";
    private static final String EXPECTED_USERNAME = "Administrator";

    private static final String MY_DASHBOARD = "//aXX[contains(@id,'default-app_my-dashboard')]";
    private static final String SITES = "//button[contains(@id,'default-app_sites') and @type='button']";

    /**
     * @param args
     */
    public static void main(String[] args)
    {
        // set properties
        System.setProperty("webdriver.firefox.bin",
                "/Users/kdesilva/Desktop/old_firefox/Firefox.app/Contents/MacOS/firefox");

        WebDriver driver = null;

        try
        {
            driver = new FirefoxDriver();
            driver.get(LOGIN_PAGE);

            // login
            WebElement userNameTxtField = driver.findElement(By.xpath(USERNAME_TEXTFIELD));
            userNameTxtField.sendKeys(USERNAME);

            WebElement passwordTxtField = driver.findElement(By.xpath(PASSWORD_TEXTFIELD));
            passwordTxtField.sendKeys(PASSWORD);

            WebElement loginBtn = driver.findElement(By.xpath(LOGIN_BUTTON));
            loginBtn.click();

            // now we are on the dashboard
            WebElement usernameElem = driver.findElement(By.xpath(USERNAME_AREA));
            String username = usernameElem.getText();

            boolean isDashboardPresent = false;
            boolean isSitesLinkPresent = false;
            try
            {
                WebElement myDashboard = driver.findElement(By.xpath(MY_DASHBOARD));
                isDashboardPresent = true;
            }
            catch (NoSuchElementException e)
            {
                System.err.println("Could not find 'My Dashboard' link");
            }

            try
            {
                WebElement sites = driver.findElement(By.xpath(SITES));
                isSitesLinkPresent = true;
            }
            catch (NoSuchElementException e)
            {
                System.err.println("Could not find 'Sites' link");
            }

            if (username.equalsIgnoreCase(EXPECTED_USERNAME) && isDashboardPresent && isSitesLinkPresent)
            {
                System.out.println("Testcase Passed!");
            }
            else
            {
                System.out.println("Testcase Failed");
            }

        }
        catch (NoSuchElementException e)
        {
            System.err.println("Could not find the following element ");
            System.err.println(e.getMessage());
        }
        catch (Exception e)
        {
            System.err.println("Unexpected Error!");
        }
        finally
        {
            if (driver != null)
            {
                driver.close();
            }
        }
    }

}
