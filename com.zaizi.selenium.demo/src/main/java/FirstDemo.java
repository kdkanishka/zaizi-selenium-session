import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class FirstDemo
{

    /**
     * @param args
     */
    public static void main(String[] args)
    {
        // set properties
        System.setProperty("webdriver.firefox.bin",
                "/Users/kdesilva/Desktop/old_firefox/Firefox.app/Contents/MacOS/firefox");

        // obtain the web driver (in preferred browser)
        WebDriver driver = new FirefoxDriver();

        // goto page
        driver.get("http://ask.com");

        String whatToSearch = "Sri Lanka";

        // find elements
        WebElement searchBox = driver.findElement(By.xpath("//input[@id='q']"));
        searchBox.sendKeys(whatToSearch);
        WebElement searchButton = driver.findElement(By.xpath("//input[@id='sbut' and @type='submit']"));
        searchButton.click();

        WebElement searchBoxOnResultPage = driver.findElement(By.xpath("//input[@id='top_q_comm' and @type='text']"));

        // logic
        String text = searchBoxOnResultPage.getAttribute("value");
        if (whatToSearch.equals(text))
        {
            System.out.println("Test case passsed");
        }

        // close web driver
        driver.close();
    }

}
